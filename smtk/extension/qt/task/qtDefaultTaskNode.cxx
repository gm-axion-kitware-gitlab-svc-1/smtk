//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/extension/qt/task/qtDefaultTaskNode.h"

#include "smtk/extension/qt/qtBaseView.h"
#include "smtk/extension/qt/task/qtTaskEditor.h"
#include "smtk/extension/qt/task/qtTaskScene.h"
#include "smtk/extension/qt/task/qtTaskViewConfiguration.h"

#include "smtk/task/Active.h"
#include "smtk/task/Manager.h"
#include "smtk/task/Task.h"

#include <QAction>
#include <QApplication>
#include <QColor>
#include <QEvent>
#include <QGraphicsProxyWidget>
#include <QGraphicsTextItem>
#include <QIcon>
#include <QLabel>
#include <QLayout>
#include <QMenu>
#include <QPainter>
#include <QTimer>

#include "task/ui_DefaultTaskNode.h"

class QAbstractItemModel;
class QItemSelection;
class QTreeView;

namespace
{

template<typename F>
/**
 * Intercept all events from a particular QObject and process them using the
 * given @c functor. This is usually used with the QObjects::installEventFilter()
 * function.
 */
class Interceptor final : public QObject
{
public:
  /**
   * Create an Interceptor that process all events of @c parent using @c functor.
   */
  Interceptor(QObject* parent, F fn)
    : QObject(parent)
    , functor(fn)
  {
  }
  ~Interceptor() override = default;

protected:
  /**
   * Filters events if this object has been installed as an event filter for the watched object.
   */
  bool eventFilter(QObject* object, QEvent* event) override { return this->functor(object, event); }

  F functor;
};

/**
 * Create a new Interceptor instance.
 */
template<typename F>
Interceptor<F>* createInterceptor(QObject* parent, F functor)
{
  return new Interceptor<F>(parent, functor);
};

} // namespace

namespace smtk
{
namespace extension
{

class DefaultTaskNodeWidget
  : public QWidget
  , public Ui::DefaultTaskNode
{
public:
  DefaultTaskNodeWidget(qtDefaultTaskNode* node, QWidget* parent = nullptr)
    : QWidget(parent)
    , m_node(node)
  {
    this->setupUi(this);
    m_title->setCheckable(true);
    QObject::connect(m_title, &QPushButton::toggled, this, &DefaultTaskNodeWidget::activateTask);
    QObject::connect(m_completed, &QCheckBox::toggled, this, &DefaultTaskNodeWidget::markCompleted);
    m_taskObserver = m_node->task()->observers().insert(
      [this](smtk::task::Task&, smtk::task::State prev, smtk::task::State next) {
        // Sometimes the application invokes this observer after the GUI
        // has been shut down. Calling setEnabled on widgets generates a
        // log message and attempting to construct a QPixmap throws exceptions;
        // so, check that qApp exists before going further.
        if (qApp)
        {
          this->updateTaskState(prev, next, m_node->isActive());
        }
      },
      "DefaultTaskNodeWidget observer");
    this->updateTaskState(m_node->m_task->state(), m_node->m_task->state(), m_node->isActive());
  }

  void flashWarning()
  {
    // TODO: pulse the widget background.
  }

  void updateTaskState(smtk::task::State prev, smtk::task::State next, bool active)
  {
    (void)prev;
    m_completed->setEnabled(m_node->m_task->editableCompletion());

    // Update the checkbox widget without infinite recursion:
    m_completed->blockSignals(true);
    m_completed->setChecked(next == smtk::task::State::Completed);
    m_completed->blockSignals(false);

    switch (next)
    {
      case smtk::task::State::Irrelevant:
      case smtk::task::State::Unavailable:
        m_title->setEnabled(false);
        break;
      case smtk::task::State::Incomplete:
      case smtk::task::State::Completable:
      case smtk::task::State::Completed:
        m_title->setEnabled(true);
        break;
    }
    m_completed->setToolTip(
      next == smtk::task::State::Completed ? "Undo completion" : "Mark completed");
    // Force the title button to match the active/inactive state of the task
    // without infinite recursion.
    m_title->blockSignals(true);
    m_title->setChecked(active);
    m_title->blockSignals(false);
    m_title->setToolTip(QString::fromStdString("Status: " + smtk::task::stateName(next)));
    m_title->setIcon(this->renderStatusIcon(next, m_title->height() / 2));
  }

  void activateTask()
  {
    auto* taskManager = m_node->m_task->manager();
    if (taskManager)
    {
      auto* previouslyActive = taskManager->active().task();
      if (previouslyActive != m_node->m_task)
      {
        if (!taskManager->active().switchTo(m_node->m_task))
        {
          this->flashWarning();
        }
      }
      else
      {
        // Deactivate this task without making any other active.
        m_title->setChecked(false);
        if (!taskManager->active().switchTo(nullptr))
        {
          this->flashWarning();
        }
      }
    }
  }

  void markCompleted()
  {
    m_node->m_task->markCompleted(m_node->m_task->state() == smtk::task::State::Completable);
    // TODO: Provide feedback if no action taken (e.g., flash red)
  }

  QIcon renderStatusIcon(smtk::task::State state, int radius)
  {
    if (radius < 10)
    {
      radius = 10;
    }
    QPixmap pix(radius, radius);
    pix.fill(QColor(0, 0, 0, 0));

    auto& cfg = *m_node->m_scene->configuration();
    QPainter painter(&pix);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBrush(QBrush(cfg.colorForState(state)));
    painter.drawEllipse(1, 1, radius - 2, radius - 2);
    painter.end();
    return QIcon(pix);
  }

  qtDefaultTaskNode* m_node;
  smtk::task::Task::Observers::Key m_taskObserver;
};

qtDefaultTaskNode::qtDefaultTaskNode(
  qtTaskScene* scene,
  smtk::task::Task* task,
  QGraphicsItem* parent)
  : Superclass(scene, task, parent)
  , m_container(new DefaultTaskNodeWidget(this))
{
  qtTaskViewConfiguration& cfg(*m_scene->configuration());
  // Create a container to hold node contents
  {
    m_container->setObjectName("nodeContainer");
    m_container->setMinimumWidth(cfg.nodeWidth());

    // install resize event filter
    m_container->installEventFilter(
      createInterceptor(m_container, [this](QObject* /*object*/, QEvent* event) {
        if (event->type() == QEvent::LayoutRequest)
        {
          this->updateSize();
        }
        return false;
      }));

    auto* graphicsProxyWidget = new QGraphicsProxyWidget(this);
    graphicsProxyWidget->setObjectName("graphicsProxyWidget");
    graphicsProxyWidget->setWidget(m_container);
    graphicsProxyWidget->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    graphicsProxyWidget->setPos(QPointF(0, 0));

    m_container->m_title->setText(QString::fromStdString(m_task->title()));
  }

  this->addToScene();

  // === Task-specific constructor ===
  this->setZValue(cfg.nodeLayer());
}

void qtDefaultTaskNode::setContentStyle(ContentStyle cs)
{
  m_contentStyle = cs;
  switch (cs)
  {
    case ContentStyle::Minimal:
      m_container->hide();
      break;
    case ContentStyle::Summary:
    case ContentStyle::Details:
      m_container->show();
      break;
    default:
      break;
  }
}

void qtDefaultTaskNode::setOutlineStyle(OutlineStyle os)
{
  qtTaskViewConfiguration& cfg(*m_scene->configuration());
  m_outlineStyle = os;
  this->setZValue(cfg.nodeLayer() - (os == OutlineStyle::Normal ? 0 : 1));
  this->update(this->boundingRect());
}

QRectF qtDefaultTaskNode::boundingRect() const
{
  qtTaskViewConfiguration& cfg(*m_scene->configuration());
  const auto& border = cfg.nodeBorderThickness();
  const double height = m_container->height();
  // was = m_headlineHeight + (m_container->isVisible() ? m_container->height() : 0.0);
  return QRectF(0, 0, m_container->width(), height).adjusted(-border, -border, border, border);
}

void qtDefaultTaskNode::paint(
  QPainter* painter,
  const QStyleOptionGraphicsItem* option,
  QWidget* widget)
{
  (void)option;
  (void)widget;
  qtTaskViewConfiguration& cfg(*m_scene->configuration());
  QPainterPath path;
  // Make sure the whole node is redrawn to avoid artifacts:
  const double borderOffset = 0.5 * cfg.nodeBorderThickness();
  const QRectF br =
    this->boundingRect().adjusted(borderOffset, borderOffset, -borderOffset, -borderOffset);
  path.addRoundedRect(br, cfg.nodeBorderThickness(), cfg.nodeBorderThickness());

  const QColor baseColor = QApplication::palette().window().color();
  const QColor highlightColor = QApplication::palette().highlight().color();
  const QColor contrastColor = QColor::fromHslF(
    baseColor.hueF(),
    baseColor.saturationF(),
    baseColor.lightnessF() > 0.5 ? baseColor.lightnessF() - 0.5 : baseColor.lightnessF() + 0.5);
  // const QColor greenBaseColor = QColor::fromHslF(0.361, 0.666, baseColor.lightnessF() * 0.4 + 0.2);

  QPen pen;
  pen.setWidth(cfg.nodeBorderThickness());
  switch (m_outlineStyle)
  {
    case OutlineStyle::Normal:
      pen.setBrush(contrastColor);
      break;
    case OutlineStyle::Active:
      pen.setBrush(highlightColor);
      break;
    default:
      break;
  }

  painter->setPen(pen);
  painter->fillPath(path, baseColor);
  painter->drawPath(path);
}

int qtDefaultTaskNode::updateSize()
{
  this->prepareGeometryChange();

  m_container->resize(m_container->layout()->sizeHint());
  Q_EMIT this->nodeResized();

  return 1;
}

void qtDefaultTaskNode::updateTaskState(smtk::task::State prev, smtk::task::State next, bool active)
{
  m_container->updateTaskState(prev, next, active);
}

} // namespace extension
} // namespace smtk
