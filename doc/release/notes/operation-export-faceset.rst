Operation System
----------------

Export Selected Faceset as an STL file
~~~~~~~~~~~~~~~~~~

A new operation (:smtk:`smtk::geometry::ExportFaceset`) has been added that
extracts the faceset of an associated component in a resource and exports
the same to a specified STL file.

In the user-interface, this operation has a custom view that shows a tree
view of the loaded resources, from which a single component can be selected
for export.
